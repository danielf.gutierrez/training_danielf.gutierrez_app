import { Component, OnInit } from '@angular/core';
import { RouterLink, Router } from '@angular/router';
import{AuthService} from '../auth.service';
import { templateSourceUrl } from '@angular/compiler';
import { invalid } from '@angular/compiler/src/render3/view/util';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalid = true;
  constructor(private router: Router,private Auth: AuthService) { }

  ngOnInit() {
  }
  loginUser(event){
    event.preventDefault()
    var valid = [['daniel','3'],['jorge','2'],['a','1'],['jonathan','4']]
    const target = event.target
    const username = target.querySelector('#username').value.toLowerCase()
    const password = target.querySelector('#password').value
    var flag = true
    for (let i = 0; i< valid.length;i++){
    if (username == valid[i][0] && password == valid[i][1] ){
      this.router.navigate(['admin']);
      flag = false
    }}
    if(flag == true) {
  
       this.invalid =false;
  
      
    }
    
    console.log(username,password)
  }

}
